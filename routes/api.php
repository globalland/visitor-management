<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('users', 'usersApiController@getAllUsers');
Route::get('users/{id}', 'usersApiController@getUser');
Route::post('users', 'usersApiController@createUser');
Route::put('users/{id}', 'usersApiController@updateUser');
Route::delete('users/{id}', 'usersApiController@deleteUser');

Route::post('login', 'cookieController@loginUser');
Route::get('logout', 'cookieController@logoutUser');
Route::get('logged', 'cookieController@checkifLoggedIn');

Route::get('cookie', 'cookieController@getAllSession');
Route::post('cookie', 'cookieController@setSession');

Route::get('types', 'typeApiController@index');
Route::get('paginatedtypes', 'typeApiController@getPaginatedTypes');
Route::get('types/{id}', 'typeApiController@show');
Route::post('types', 'typeApiController@create');
Route::put('types/{id}', 'typeApiController@update');
Route::delete('types/{id}', 'typeApiController@delete');

Route::get('buildings', 'buildingApiController@index');
Route::get('paginatedbuildings', 'buildingApiController@getPaginatedBuildings');
Route::get('buildings/{id}', 'buildingApiController@show');
Route::post('buildings', 'buildingApiController@create');
Route::put('buildings/{id}', 'buildingApiController@update');
Route::delete('buildings/{id}', 'buildingApiController@delete');

Route::get('tovisits', 'typeOfVisitApiController@index');
Route::get('paginatedvisits', 'typeOfVisitApiController@getPaginatedVisits');
Route::get('tovisits/{id}', 'typeOfVisitApiController@show');
Route::post('tovisits', 'typeOfVisitApiController@create');
Route::put('tovisits/{id}', 'typeOfVisitApiController@update');
Route::delete('tovisits/{id}', 'typeOfVisitApiController@delete');

Route::get('guests', 'guestApiController@index');
Route::get('guests/{id}', 'guestApiController@show');
Route::post('guests', 'guestApiController@create');
Route::put('guests/{id}', 'guestApiController@update');
Route::delete('guests/{id}', 'guestApiController@delete');