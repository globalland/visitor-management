<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function(){
    return view ('dashboard');
});

Route::get('/users', function(){
    return view ('users');
});

Route::get('/usertypes', function(){
    return view ('usertype');
});

Route::get('/buildings', function(){
    return view ('building');
});

Route::get('/visits', function(){
    return view ('tovisits');
});


Route::get('/register', function(){
    return view ('register');
});
