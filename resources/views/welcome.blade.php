<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Login</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href=" {{ asset('css/bootstrap.css') }}" rel="stylesheet">

        <!-- Styles -->
        <style>
        .cent {
            margin: auto;
            width: 50%;
            text-align:center;
            padding: 10px;
        }
        </style>
    </head>
    <body>
        <div id="app">
        <div class="container cent">
        <login>
        </login>
        </div>
        </div>
        <script src=" {{ asset('js/app.js') }}"></script>
        <script src=" {{ asset('bootstrap/js/bootstrap.js') }}"></script>
    </body>
</html>
