<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Guest Registration</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href=" {{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href=" {{ asset('css/register.css') }}" rel="stylesheet">
        <!-- Styles -->

    </head>
    <body>
        <div id="app">
        <div class="content">
            <register></register>
        </div>
        </div>
        <script src=" {{ asset('js/app.js') }}"></script>
        <script src=" {{ asset('bootstrap/js/bootstrap.js') }}"></script>
    </body>
</html>
