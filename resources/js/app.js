
import Vue from 'vue';

window.Vue = require('vue');

Vue.component('login', require('./components/Login.vue').default);
Vue.component('dash', require('./components/Dash.vue').default);
Vue.component('sidebar', require('./components/Sidebar.vue').default);
Vue.component('navbar', require('./components/Navbar.vue').default);
Vue.component('user',  require('./components/User.vue').default);
Vue.component('usertype', require('./components/UserType.vue').default);
Vue.component('building', require('./components/Building.vue').default);
Vue.component('tovisit', require('./components/TypeOfVisit.vue').default);
Vue.component('register', require('./components/Register.vue').default);

const app = new Vue({
    el: '#app'
});