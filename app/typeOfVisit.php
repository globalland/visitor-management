<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class typeOfVisit extends Model
{   
    protected $primaryKey = 'TypeOfVisitID';

    protected $fillable = [
        'TypeOfVisitID',
        'TypeOfVisit'
    ];
}
