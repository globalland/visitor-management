<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userType extends Model
{

    protected $primaryKey = 'UserTypeID';
    protected $fillable = [
        'UserTypeID',
        'UserType',
        'Description',
        'created_at'
    ];
}
