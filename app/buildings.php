<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class buildings extends Model
{
    protected $primaryKey = 'BuildingID';

    protected $fillable = [
        'BuildingID',
        'BuildingName',
        'Description',
        'created_at'
    ];
}