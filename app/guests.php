<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class guests extends Model
{
    protected $primaryKey = "GuestID";

    protected $fillable = [
        'GuestName',
        'Mobile',
        'Email',
        'TypeOfVisitID',
        'CheckInDate',
        'CheckOutDate',
        'ImagePath',
        'Location'
    ];
}
