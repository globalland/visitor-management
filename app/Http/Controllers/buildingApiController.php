<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\buildings;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Exception;
use App\Users;
use Cookie;

class buildingApiController extends Controller
{
    //get all buildings
    public function index()
    {
        $building = buildings::all();
        return response($building, 200);
    }
    //get all buildings with pagination
    public function getPaginatedBuildings()
    {
        $building = buildings::paginate();
        return response($building, 200);
    }
     //add new buildings to the database
    public function create(Request $request)
    {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            $building = new Buildings;
    
        $validator = Validator::make($request->all(), [
            'BuildingName' => 'required',
            'Description' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json($validator->messages());
        } else {
                try {
                $building->BuildingName = $request->BuildingName;
                $building->Description = $request->Description;
                $building->created_at = date('Y-m-d');
                $building->updated_at = null;
                $building->save();
                } catch(\Exception $e) {
                    return response()->json([
                        "Error" => "Building already exists!"
                    ], 201);
                }
                return response()->json([
                    "Success" => "Building Successfully Added!"
                ], 201);
        }
    }
}
     //update existing buildings from the database
    public function update(Request $request)
    {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            $validator = Validator::make($request->all(), [
                'BuildingName' => 'required',
                'Description' => 'required',
            ]);
    
            if($validator->fails()) {
                return response()->json($validator->messages());
            } else {
                if(Buildings::where('BuildingID', $request->BuildingID)->exists()) {
                    try {
                    $building = Buildings::find($request->BuildingID);
                    $building->BuildingName = $request->BuildingName;
                    $building->Description = $request->Description;
                    $building->updated_at = date('Y-m-d');
                    $building->save();

                    return response()->json([
                        "Success" => "User Updated"
                    ], 200);
                    } catch(\Exception $e) {
                        return response()->json($e);
                        $exceptionMessage = $e->getMessage();
                        $usert = false;

                        if(Str::contains($exceptionMessage, 'BUILDING')) {
                            $usert = true;
                        }
                        return response()->json([
                            "Building" => $usert
                        ]);
                    }
        
                    ;
                } else {
                    return response()->json([
                        "message" => "Error Updating"
                    ]);
        
                }
            }
        }
    }
     //delete buildings from database
    public function delete($id)
    {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            if(Buildings::where('BuildingID', $id)->exists()) {
                $Buildings = Buildings::find($id);
                $Buildings->delete();
    
                return response()->json([
                    "message" => "User Deleted"
                ], 200);
            } else {
                return response()->json([
                    "message" => "Error Deleting"
                ], 404);
            }
        }
    }
}
