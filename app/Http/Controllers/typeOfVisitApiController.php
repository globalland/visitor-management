<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Exception;
use App\Users;
use Cookie;
use App\typeOfVisit;

class typeOfVisitApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $visits = typeOfVisit::all();
        return response($visits, 200);
    }

    public function getPaginatedVisits() {
        $visits = typeOfVisit::paginate();
        return response($visits, 200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            $typeOfVisit = new typeOfVisit;
    
        $validator = Validator::make($request->all(), [
            'TypeOfVisit' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->messages());
        } else {
                try {
                $typeOfVisit->TypeOfVisit = $request->TypeOfVisit;
                $typeOfVisit->created_at = date('Y-m-d');
                $typeOfVisit->updated_at = null;
                $typeOfVisit->save();
                } catch(\Exception $e) {
                    return response()->json([
                        "Error" => "Visit Type already exists!"
                    ], 201);
                }
                return response()->json([
                    "Success" => "Visit Type Successfully Added!"
                ], 201);
        }
    }
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            $validator = Validator::make($request->all(), [
                'TypeOfVisit' => 'required',
            ]);
    
            if($validator->fails()) {
                return response()->json($validator->messages());
            } else {
                if(TypeOfVisit::where('TypeOfVisitID', $request->TypeOfVisitID)->exists()) {
                    try {
                    $usertype = TypeOfVisit::find($request->TypeOfVisitID);
                    $usertype->TypeOfVisit = $request->TypeOfVisit;
                    $usertype->updated_at = date('Y-m-d');
                    $usertype->save();

                    return response()->json([
                        "Success" => "User Updated"
                    ], 200);
                    } catch(\Exception $e) {
                        return response()->json($e);
                        $exceptionMessage = $e->getMessage();
                        $usert = false;

                        if(Str::contains($exceptionMessage, 'TYPEOFVISIT')) {
                            $usert = true;
                        }
                        return response()->json([
                            "typeOfVisit" => $usert
                        ]);
                    }
        
                    ;
                } else {
                    return response()->json([
                        "message" => "Error Updating"
                    ]);
        
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            if(TypeOfVisit::where('TypeOfVisitID', $id)->exists()) {
                $userType = TypeOfVisit::find($id);
                $userType->delete();
    
                return response()->json([
                    "message" => "User Deleted"
                ], 200);
            } else {
                return response()->json([
                    "message" => "Error Deleting"
                ], 404);
            }
        }
    }
}
