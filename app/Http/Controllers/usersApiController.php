<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Exception;
use App\Users;
use App\UserType;
use App\Buildings;

use Cookie;

class usersApiController extends Controller
{

    //get all users
    public function getAllUsers() {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            if(Cookie::get('auth_token')) {
            $users = Users::paginate();
            foreach($users as $user) {
                $user->UserTypeData = UserType::find($user->UserTypeID);
                if($user->UserTypeData == null) {
                    $user->UserTypeData = [
                        'UserType' => 'none'
                    ];
                };
                $user->BuildingData = Buildings::find($user->BuildingID);
                if($user->BuildingData == null) {
                    $user->BuildingData = [
                        'BuildingName' => 'none'
                    ];
                };
            }
            return response($users, 200);
            }
        }
    }

    //add new user
    public function createUser(Request $request) {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            $user = new Users;
    
        $validator = Validator::make($request->all(), [
            'Username' => 'required',
            'Password' => 'required',
            'Email' => 'required|email',
            'Contact' => 'required',
            'UserTypeID' => 'required',
            'BuildingID' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->messages());
        } else {
                try {
$user->Username = $request->Username;
                $user->Password = Hash::make($request->Password);
                $user->Email = $request->Email;
                $user->Contact = $request->Contact;
                $user->UserTypeID = $request->UserTypeID;
                $user->BuildingID = $request->BuildingID;
                $user->created_at = date('Y-m-d');
                $user->updated_at = null;
                $user->save();
                } catch(\Exception $e) {
                    return response()->json([
                        "Error" => "User already exists!"
                    ], 201);
                }
                return response()->json([
                    "Success" => "User Successfully Added!"
                ], 201);
        }
        }
    }
    
    //get a single user
    public function getUser($id) {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            $user = Users::where('UserID', $id)->get();
            return response($user, 200);
        } else {
            return response()->json([
                "message" => "Not Found"
            ], 404);
        }
    }

    //update existing user
    public function updateUser(Request $request) {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            $validator = Validator::make($request->all(), [
                'Username' => 'required',
                'Password' => 'required',
                'Email' => 'required|email',
                'Contact' => 'required',
                'UserTypeID' => 'required',
                'BuildingID' => 'required'
            ]);
    
            if($validator->fails()) {
                return response()->json($validator->messages());
            } else {
                if(Users::where('UserID', $request->UserID)->exists()) {
                    try {
                    $user = Users::find($request->UserID);
                    $user->Username = $request->Username;
                    if(strlen($request->Password) == 60) {
                    } else {
                        $user->Password = Hash::make($request->Password);
                    }
                    $user->Email = $request->Email;
                    $user->Contact = $request->Contact;
                    $user->UserTypeID = $request->UserTypeID;
                    $user->BuildingID = $request->BuildingID;
                    $user->updated_at = date('Y-m-d');
                    $user->save();

                    return response()->json([
                        "Success" => "User Updated"
                    ], 200);
                    } catch(\Exception $e) {
                        $exceptionMessage = $e->getMessage();
                        $email = false;
                        $username = false;

                        if(Str::contains($exceptionMessage, 'EMAIL')) {
                            $email = true;
                        } else if(Str::contains($exceptionMessage, 'Username')) {
                            $username = true;
                        }
                        return response()->json([
                            "Email" => $email,
                            "Username" => $username
                        ]);
                    }
        
                    ;
                } else {
                    return response()->json([
                        "message" => "Error Updating"
                    ]);
        
                }
            }
        }
    }
    //delete user 
    public function deleteUser($id) {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            if(Users::where('UserID', $id)->exists()) {
                $user = Users::find($id);
                $user->delete();
    
                return response()->json([
                    "message" => "User Deleted"
                ], 200);
            } else {
                return response()->json([
                    "message" => "Error Deleting"
                ], 404);
            }
        }
    }
}