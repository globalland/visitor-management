<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserType;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Exception;
use App\Users;
use Cookie;

class typeApiController extends Controller
{

    //get all user types
    public function index()
    {
        $types = userType::all();
        return response($types, 200);
    }

    //get paginated user types
    public function getPaginatedTypes()
    {
        $types = userType::paginate();
        return response($types, 200);
    }

    //add new user type
    public function create(Request $request)
    {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            $userType = new userType;
    
        $validator = Validator::make($request->all(), [
            'UserType' => 'required',
            'Description' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json($validator->messages());
        } else {
                try {
                $userType->UserType = $request->UserType;
                $userType->Description = $request->Description;
                $userType->created_at = date('Y-m-d');
                $userType->updated_at = null;
                $userType->save();
                } catch(\Exception $e) {
                    return response()->json([
                        "Error" => "User Type already exists!"
                    ], 201);
                }
                return response()->json([
                    "Success" => "User Type Successfully Added!"
                ], 201);
        }
    }
}
    //update existing user type
    public function update(Request $request)
    {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            $validator = Validator::make($request->all(), [
                'UserType' => 'required',
                'Description' => 'required',
            ]);
    
            if($validator->fails()) {
                return response()->json($validator->messages());
            } else {
                if(UserType::where('UserTypeID', $request->UserTypeID)->exists()) {
                    try {
                    $usertype = UserType::find($request->UserTypeID);
                    $usertype->UserType = $request->UserType;
                    $usertype->Description = $request->Description;
                    $usertype->updated_at = date('Y-m-d');
                    $usertype->save();

                    return response()->json([
                        "Success" => "User Updated"
                    ], 200);
                    } catch(\Exception $e) {
                        return response()->json($e);
                        $exceptionMessage = $e->getMessage();
                        $usert = false;

                        if(Str::contains($exceptionMessage, 'UserType')) {
                            $usert = true;
                        }
                        return response()->json([
                            "UserType" => $usert
                        ]);
                    }
        
                    ;
                } else {
                    return response()->json([
                        "message" => "Error Updating"
                    ]);
        
                }
            }
        }
    }

    //delete user type
    public function delete($id)
    {
        $token = Cookie::get('auth_token');
        if(Users::where('auth_token', $token)->exists()) {
            if(UserType::where('UserTypeID', $id)->exists()) {
                $userType = UserType::find($id);
                $userType->delete();
    
                return response()->json([
                    "message" => "User Deleted"
                ], 200);
            } else {
                return response()->json([
                    "message" => "Error Deleting"
                ], 404);
            }
        }
    }
}
