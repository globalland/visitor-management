<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Users;
use Cookie;

class cookieController extends Controller
{
     //implementation of getter of cookie
    public function getCookie($id) {
        return response()->json([
            'data' => Cookie::get($id)
        ], 200);
    }
    //implementation of setter of cookie
    public function setCookie($key, $value) {
        $cookie = Cookie::forever($key, $value);
        return $cookie;
    }
    //check if the user is logged in
    public function checkifLoggedIn() {
        $token = Cookie::get('auth_token');
        if($token !== null) {
            $user = Users::where('auth_token', $token)->first();
            return response()->json([
                'data' => true,
                'auth_token' => Cookie::get('auth_token'),
                'UserID' => $user->UserID
            ], 200);
        } else {
            return response()->json([
                'data' => false
            ]);
        }
    }
    //log out the user
    public function logoutUser() {
        

        return response()->json([
            'data' => true
        ])->withCookie(Cookie::forget('auth_token'));
    }

    //login user with validation and checker
    public function loginUser(Request $request) {

        $validator = Validator::make($request->all(), [
            'Email' => 'required|email',
            'Password' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            if(Users::where('Email', $request->Email)->exists()) {
                $user = Users::where('Email', $request->Email)->first();
                if (Hash::check($request->Password, $user->Password)) {
                    $userAuthenticated = Users::find($user->UserID);
                    $auth_token = Str::random(60);
                    $userAuthenticated->auth_token = $auth_token;
                    $userAuthenticated->save();
            
                    return response()->json([
                        "Success" => "logged"
                    ], 200)->withCookie(Cookie::forever('auth_token', $auth_token));
                } else {
                    return response()->json([
                        "Error" => "Incorrect username and password."
                    ], 200);
                }
            } else {
                return response()->json([
                    "Error" => "Account does not exist."
                ], 200);
            }
        }
    }
}
