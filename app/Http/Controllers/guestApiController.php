<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\guests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class guestApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $guestData = json_decode($request->guestData);
        
        $validator = Validator::make(json_decode($request->guestData,true), [
            'GuestName' => 'required',
            'Mobile' => 'required',
            'Email' => 'required|email',
            'TypeOfVisitID' => 'required',
            'Location' => 'required'
        ]);
        
        if($validator->fails()) {
            return response()->json($validator->messages());
        } else {
        $guest = new guests;
        
        $guest->GuestName = $guestData->GuestName;
        $guest->Mobile = $guestData->Mobile;
        $guest->Email = $guestData->Email;
        $guest->TypeOfVisitID = $guestData->TypeOfVisitID;
        $guest->CheckInDate = date('Y/m/d H:i:s A');
        $guest->CheckOutDate = null;
        $guest->ImagePath = '';
        $guest->Location = $guestData->Location;

        try {
            $guest->save();
            return response()->json([
                'success' => 'message'
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'error' => 'message'
            ]);
        }
        }
            
        /* $fileData = $request->file('file');

        $path = public_path() . '/uploads/';

        if(Str::contains($fileData->getMimeType(), 'image')) {
            $fileData->move($path, $fileData->getClientOriginalName());
        } */
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
