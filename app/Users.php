<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $primaryKey = 'UserID';

    protected $fillable = [
        'Username', 
        'Password', 
        'Email',
        'Contact',
        'UserTypeID',
        'BuildingID',
        'created_at',
        'updated_at'
    ];
}
